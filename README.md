#Resilience Partnership Evaluation Task

### Story 1

As a system administrator, I want to receive an email notification whenever a new site group is created, so that I'm made aware of new site groups 
 
• When a new group of type site (not user and not organization) is created on the platform, all system administrators will receive an email with the group name, group url and the name of the user that created the group. 

• The email template (both subject and body fields of the email) should be easily updatable by system admins. 

• Email content is localized based on the receiver’s user preferred language 

• Technical suggestion: Use global web content to manage email template as it supports localisation

### Story 2

As a user, I want to receive an out of office response when someone I'm trying to contact is away, so that I know they're not going to respond quickly 
     
• On the user profile page (in the user’s personal site) a new portlet is added to allow the user to configure their out of office response. 

• Users can only update their own out of office response. 

• Out of office response has a start date and time, end date and time and personal message (html). 

• When userA sends a private message (using Liferay’s private messaging functionality) to userB, who is currently out of the office, the message will be sent to userB (normal behaviour) and userA will automatically receive the configured out of office reply.
    
###Story 3 

As a user, I want the activity stream to show activities related to likes on comments • When a user likes a comment (Liferay’s rating system), a new activity feed entry should be displayed in the Liferay activity stream portlet in the format: 

• {userA} liked {userB}’s comment on {contentType}, <a href=”{link to content full view}>{content title}</a>, in <a href=”{groupUrl}>{groupName}</a> 

• John liked Simon’s comment on document, My example document, in My test group 

• John liked Simon’s comment on blog, My example blog, in My test group

________________________________________________________________________________________________________________________


### Implementation details

Liferay: `7.2-ga1`

Java: `1.8`

IDE: `IntelliJ IDEA 2018.2.5`

Liferay IntelliJ Plugin: `1.5.0`

DB: `MySQL 5.7.26`


### Estimates

#### Story 1

- Group model listener implementation - 3h

- Notification Sender API implementation - 1h

- Notification Sender implementation (with web content approach) - 6h

- Bundle Activator (for automatic web content creation) - 4h

Total: 14h

#### Story 2

- Custom portlet implementation - 8h

- Listener on message creation (sending) implementation - 4h

- Sending out-of-office response implementation - 4h

Total: 16h

#### Story 3

- Ratings entry listener implementation - 6h

- Resource bundle hook implementation - 2h

- Social activity interpreters implementation - 10h

Total: 18h

_Note: Code provided for Story 1 and Story 3_


### Demo Server

http://liferay-demo.aimprosoft.com/web/placecube

test@liferay.com / test