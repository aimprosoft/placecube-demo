package com.aimprosoft.notification.util.mock;

import com.liferay.portal.kernel.model.GroupWrapper;

public class GroupMock extends GroupWrapper {

    private final long groupId;
    private final int type;

    public GroupMock(long groupId, int type) {
        super(null);
        this.groupId = groupId;
        this.type = type;
    }

    @Override
    public long getGroupId() {
        return groupId;
    }

    @Override
    public int getType() {
        return type;
    }
}
