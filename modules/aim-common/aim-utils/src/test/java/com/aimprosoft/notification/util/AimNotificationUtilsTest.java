package com.aimprosoft.notification.util;

import com.aimprosoft.notification.util.mock.GroupMock;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.GroupConstants;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import static org.assertj.core.api.Assertions.assertThat;

@MockitoSettings(strictness = Strictness.LENIENT)
@ExtendWith(MockitoExtension.class)
public class AimNotificationUtilsTest {

    private static final long GROUP_ID = 10112;

    @Test
    public void isSite_WithNullGroup_ShouldReturn_False(){
        Group group = null;
        boolean isSite = AimNotificationUtils.isSite(group);
        assertThat(isSite).isFalse();
    }

    @Test
    public void isSite_WithTypeSite_ShouldReturn_True(){
        Group group = new GroupMock(GROUP_ID, GroupConstants.TYPE_SITE_OPEN);
        boolean isSite = AimNotificationUtils.isSite(group);
        assertThat(isSite).isTrue();
    }

    @Test
    public void prepareContent_WithoutExtraInfo_ShouldReturn_TheSameContent() {
        String originalHtmlContent = "<p>Demo mail template</p>";
        String preparedContent = AimNotificationUtils.prepareContent(originalHtmlContent);
        assertThat(preparedContent).isEqualTo(originalHtmlContent);
    }


}
