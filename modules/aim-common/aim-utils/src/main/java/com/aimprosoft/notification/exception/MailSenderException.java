package com.aimprosoft.notification.exception;

/**
 *  Exception during sending email notification
 *
 */
public class MailSenderException extends Exception {

    public MailSenderException() {
    }

    public MailSenderException(String message) {
        super(message);
    }

    public MailSenderException(String message, Throwable cause) {
        super(message, cause);
    }

    public MailSenderException(Throwable cause) {
        super(cause);
    }
}
