package com.aimprosoft.notification.util;

import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.GroupConstants;
import com.liferay.portal.kernel.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.util.ArrayList;
import java.util.List;

import static com.aimprosoft.notification.util.AimNotificationConstants.KEY_GROUP_NAME;
import static com.aimprosoft.notification.util.AimNotificationConstants.KEY_GROUP_URL;
import static com.aimprosoft.notification.util.AimNotificationConstants.KEY_USER_NAME;

public class AimNotificationUtils {

    private static final String WC_START_TAG = "<dl><dt>content:</dt><dd>";
    private static final String WC_END_TAG = "</dd></dl>";
    private static final String EMPTY = "";

    /**
     * Checks if Liferay group is site
     *
     * @param group Group
     * @return true, if group is site
     */
    public static boolean isSite(Group group) {
        if (group == null) {
            return false;
        }
        int type = group.getType();
        return type == GroupConstants.TYPE_SITE_OPEN ||
                type == GroupConstants.TYPE_SITE_PRIVATE ||
                type == GroupConstants.TYPE_SITE_RESTRICTED ||
                type == GroupConstants.TYPE_SITE_SYSTEM;
    }

    /**
     * Removed extra "content:" from content html
     *
     * @param articleContent - original content
     * @return prepared content
     */
    public static String prepareContent(String articleContent){
        return StringUtil.replace(
                articleContent,
                new String[]{
                        WC_START_TAG,
                        WC_END_TAG
                },
                new String[]{
                        EMPTY,
                        EMPTY
                }
        );
    }

    /**
     * Fills email template with parameters
     *
     * @param template email template
     * @param groupName name of site group
     * @param groupUrl site URL
     * @param userName user full name
     *
     * @return - email
     */
    public static String fillTemplateParams(String template, String groupName, String groupUrl, String userName) {
        return StringUtil.replace(
                template,
                new String[]{
                        KEY_GROUP_NAME,
                        KEY_GROUP_URL,
                        KEY_USER_NAME
                },
                new String[]{
                        groupName,
                        groupUrl,
                        userName
                }
        );
    }

    /**
     * Converts email list to InternetAddress array
     *
     * @param emailAddresses email address list
     *
     * @return InternetAddress array
     */
    public static InternetAddress[] toInternetAddress(List<String> emailAddresses){
        List<InternetAddress> internetAddresses = new ArrayList<>();
        for (String emailAddress : emailAddresses) {
            try {
                InternetAddress internetAddress = new InternetAddress(emailAddress);
                internetAddresses.add(internetAddress);
            } catch (AddressException e) {
                _log.error("Can not create InternetAddress from email: " + emailAddress + ", caused: " + e.getMessage());
            }
        }
        return internetAddresses.toArray(new InternetAddress[0]);
    }

    private static Logger _log = LoggerFactory.getLogger(AimNotificationUtils.class.getName());


}