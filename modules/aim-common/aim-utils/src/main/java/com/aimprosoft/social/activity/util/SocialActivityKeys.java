package com.aimprosoft.social.activity.util;

public class SocialActivityKeys  {

    public static final int TYPE_LIKE_ON_COMMENT = 0;

    public static final String DEFAULT_EXTRA_DATA = "";

    public static final String WIKI_ACTIVITY_INTERPRETER = "com.liferay.wiki.web.internal.social.WikiActivityInterpreter";
    public static final String BLOGS_ACTIVITY_INTERPRETER = "com.liferay.blogs.web.internal.social.BlogsActivityInterpreter";
    public static final String DL_FILE_ACTIVITY_INTERPRETER = "com.liferay.document.library.web.internal.social.DLFileEntryActivityInterpreter";

}
