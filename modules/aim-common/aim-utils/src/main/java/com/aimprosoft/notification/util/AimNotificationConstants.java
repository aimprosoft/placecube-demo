package com.aimprosoft.notification.util;

public class AimNotificationConstants {

    // Web Content URL for "Group Created" email notification
    public static final String SITE_GROUP_TEMPLATE_WC_URL = "site-group-created-email-template";

    // Web Content Structure/Template for "Group Created" email notification
    public static final String WC_STRUCTURE_KEY = "BASIC-WEB-CONTENT";
    public static final String WC_TEMPLATE_KEY = null;


    // Email template parameters
    public static final String KEY_GROUP_NAME = "[$GROUP_NAME$]";
    public static final String KEY_GROUP_URL = "[$GROUP_URL$]";
    public static final String KEY_USER_NAME = "[$USER_NAME$]";

}