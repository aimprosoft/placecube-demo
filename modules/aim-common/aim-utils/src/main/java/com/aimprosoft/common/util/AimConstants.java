package com.aimprosoft.common.util;

import com.liferay.portal.kernel.util.LocaleUtil;

import java.util.Locale;

public class AimConstants {

    // Liferay company MX
    public static final String COMPANY_MX = "liferay.com";

    // Default language ID
    public static final String DEFAULT_LANGUAGE_ID = "en_US";

    // Default locale
    public static final Locale DEFAULT_LOCALE = LocaleUtil.fromLanguageId(DEFAULT_LANGUAGE_ID);;


}
