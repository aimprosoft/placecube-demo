package com.aimprosoft.email.notification;

import com.aimprosoft.email.notification.api.EmailNotificationSender;
import com.aimprosoft.notification.exception.MailSenderException;
import com.aimprosoft.notification.util.AimNotificationUtils;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalArticleDisplay;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.mail.kernel.model.MailMessage;
import com.liferay.mail.kernel.service.MailServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.*;
import com.liferay.portal.kernel.service.*;
import com.liferay.portal.kernel.util.Constants;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.kernel.util.PropsKeys;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.internet.InternetAddress;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import static com.aimprosoft.notification.util.AimNotificationConstants.SITE_GROUP_TEMPLATE_WC_URL;

@Component(
	immediate = true,
	property = {
	},
	service = EmailNotificationSender.class
)
public class EmailNotificationSenderService implements EmailNotificationSender {

	@Override
	public void sendNotificationEmail(Group group) throws MailSenderException {
		try {
			//Portal instance ID
			long companyId = group.getCompanyId();

			//Administrator's email addresses
			List<String> adminEmailAddresses = getAdminEmailAddresses(companyId);
			if (adminEmailAddresses.size() == 0) {
				return;
			}

			//Web content with localized email template
			JournalArticleDisplay articleDisplay = getEmailTemplateArticleDisplay(companyId);
			String articleTitle = articleDisplay.getTitle();
			String articleContent = articleDisplay.getContent();
			articleContent = AimNotificationUtils.prepareContent(articleContent);

			//Prepare email template parameters
			ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();
			Locale locale = serviceContext.getLocale();
			String groupName = group.getName(locale);
			String userName = getCreatorUserName(group);
			String groupUrl = serviceContext.getPortalURL() + "/web" + group.getFriendlyURL();

			//Fill email parameters
			String mailSubject = AimNotificationUtils.fillTemplateParams(articleTitle, groupName, groupUrl, userName);
			String mailBody = AimNotificationUtils.fillTemplateParams(articleContent, groupName, groupUrl, userName);

			//Email settings
			String fromName = PrefsPropsUtil.getString(companyId, PropsKeys.ADMIN_EMAIL_FROM_NAME);
			String fromAddress = PrefsPropsUtil.getString(companyId, PropsKeys.ADMIN_EMAIL_FROM_ADDRESS);

			//Prepare mail message and send email
			MailMessage mailMessage = new MailMessage();
			mailMessage.setHTMLFormat(true);
			mailMessage.setSubject(mailSubject);
			mailMessage.setBody(mailBody);
			mailMessage.setFrom(new InternetAddress(fromAddress, fromName));
			InternetAddress[] toInternetAddresses = AimNotificationUtils.toInternetAddress(adminEmailAddresses);
			mailMessage.setTo(toInternetAddresses);

			if (_log.isInfoEnabled()) {
				_log.info("Sending 'Site Group Created' notification email to admin users: " + adminEmailAddresses);
			}

			MailServiceUtil.sendEmail(mailMessage);

			if (_log.isInfoEnabled()) {
				_log.info("Notification has been sent successfully.");
			}

		} catch (Exception e) {
			_log.info("Error during sending notification: " + e.getMessage());
			throw new MailSenderException(e);
		}
	}

	private String getCreatorUserName(Group group) throws PortalException {
		long creatorUserId = group.getCreatorUserId();
		User user = _userLocalService.getUser(creatorUserId);
		return user.getFullName();
	}

	private List<String> getAdminEmailAddresses(long companyId) throws PortalException {
		Role adminRole = _roleLocalService.getRole(companyId, RoleConstants.ADMINISTRATOR);
		long adminRoleId = adminRole.getRoleId();
		List<User> adminUsers = _userLocalService.getRoleUsers(adminRoleId);
		return adminUsers.stream().map(User::getEmailAddress).collect(Collectors.toList());
	}

	private JournalArticleDisplay getEmailTemplateArticleDisplay(long companyId) throws PortalException {
		Group globalGroup = _groupLocalService.getFriendlyURLGroup(companyId, GroupConstants.GLOBAL_FRIENDLY_URL);
		long groupGroupId = globalGroup.getGroupId();
		JournalArticle article = _journalArticleLocalService
				.getArticleByUrlTitle(groupGroupId, SITE_GROUP_TEMPLATE_WC_URL);
		ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();
		Locale locale = serviceContext.getLocale();
		String languageId = LocaleUtil.toLanguageId(locale);
		return _journalArticleLocalService.getArticleDisplay(
				article.getGroupId(),
				article.getArticleId(),
				Constants.VIEW,
				languageId,
				serviceContext.getThemeDisplay()
		);
	}


	@Reference(unbind = "-")
	public void setRoleLocalService(RoleLocalService roleLocalService) {
		this._roleLocalService = roleLocalService;
	}

	@Reference(unbind = "-")
	public void setUserLocalService(UserLocalService userLocalService) {
		this._userLocalService = userLocalService;
	}

	@Reference(unbind = "-")
	public void setGroupLocalService(GroupLocalService groupLocalService) {
		this._groupLocalService = groupLocalService;
	}

	@Reference(unbind = "-")
	public void setJournalArticleLocalService(JournalArticleLocalService journalArticleLocalService) {
		this._journalArticleLocalService = journalArticleLocalService;
	}

	private RoleLocalService _roleLocalService;
	private UserLocalService _userLocalService;
	private GroupLocalService _groupLocalService;
	private JournalArticleLocalService _journalArticleLocalService;

	private static Logger _log = LoggerFactory.getLogger(EmailNotificationSenderService.class.getName());
}