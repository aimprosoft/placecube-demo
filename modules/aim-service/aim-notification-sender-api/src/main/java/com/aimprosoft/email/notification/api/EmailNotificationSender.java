package com.aimprosoft.email.notification.api;

import com.aimprosoft.notification.exception.MailSenderException;
import com.liferay.portal.kernel.model.Group;

public interface EmailNotificationSender {

    void sendNotificationEmail(Group group) throws MailSenderException;

}