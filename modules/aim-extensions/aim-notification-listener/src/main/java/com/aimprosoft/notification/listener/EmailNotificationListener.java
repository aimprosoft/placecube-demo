package com.aimprosoft.notification.listener;

import com.aimprosoft.email.notification.api.EmailNotificationSender;
import com.aimprosoft.notification.exception.MailSenderException;
import com.aimprosoft.notification.util.AimNotificationUtils;
import com.liferay.portal.kernel.exception.ModelListenerException;
import com.liferay.portal.kernel.model.BaseModelListener;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.ModelListener;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(
	immediate = true,
	property = {
	},
	service = ModelListener.class
)
public class EmailNotificationListener extends BaseModelListener<Group> {

	@Override
	public void onAfterCreate(Group group) throws ModelListenerException {

		if (_log.isInfoEnabled()) {
			_log.info("Started EmailNotificationListener.");
		}

		//Check if group is site
		if (AimNotificationUtils.isSite(group)) {
			//Send notifications email
			try {
				_emailNotificationSender.sendNotificationEmail(group);
			} catch (MailSenderException e) {
				_log.error("Failed to send notification email after creation group #" + group.getGroupId() + ", caused: " + e.getMessage());
			}
		}

		super.onAfterCreate(group);
	}

	@Reference(unbind = "-")
	public void setEmailNotificationSender(EmailNotificationSender emailNotificationSender) {
		this._emailNotificationSender = emailNotificationSender;
	}

	private EmailNotificationSender _emailNotificationSender;

	private static Logger _log = LoggerFactory.getLogger(EmailNotificationListener.class.getName());
}