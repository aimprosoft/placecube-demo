package com.aimprosoft.ratings.listener;

import com.aimprosoft.social.activity.util.SocialActivityKeys;
import com.liferay.blogs.model.BlogsEntry;
import com.liferay.blogs.service.BlogsEntryLocalService;
import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.service.DLFileEntryLocalService;
import com.liferay.message.boards.model.MBDiscussion;
import com.liferay.message.boards.model.MBMessage;
import com.liferay.message.boards.service.MBMessageLocalService;
import com.liferay.portal.kernel.exception.ModelListenerException;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.json.JSONUtil;
import com.liferay.portal.kernel.model.BaseModelListener;
import com.liferay.portal.kernel.model.GroupedModel;
import com.liferay.portal.kernel.model.ModelListener;
import com.liferay.portal.kernel.social.SocialActivityManagerUtil;
import com.liferay.ratings.kernel.model.RatingsEntry;
import com.liferay.wiki.model.WikiPage;
import com.liferay.wiki.service.WikiPageLocalService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(
        immediate = true,
        property = {
        },
        service = ModelListener.class
)
public class RatingsEntryListener extends BaseModelListener<RatingsEntry> {

    @Override
    public void onAfterCreate(RatingsEntry ratingsEntry) throws ModelListenerException {

        try {

            String className = ratingsEntry.getClassName();
            if (!MBDiscussion.class.getName().equals(className)) {
                //Liked not a comment, skipping
                return;
            }

            //User ID, who liked a comment
            long likeUserId = ratingsEntry.getUserId();

            long messageId = ratingsEntry.getClassPK();
            MBMessage mbMessage = _mbMessageLocalService.getMessage(messageId);

            //User ID, who commented
            long commentUserId = mbMessage.getUserId();

            String modelClassName = mbMessage.getClassName();
            long modelClassPK = mbMessage.getClassPK();

            //Get entity by className and classPK
            GroupedModel entity = null;
            String entityTitle = "";
            if (BlogsEntry.class.getName().equals(modelClassName)) {
                //Blog
                entity = _blogsEntryLocalService.fetchBlogsEntry(modelClassPK);
                entityTitle = ((BlogsEntry) entity).getTitle();
            } else if (WikiPage.class.getName().equals(modelClassName)) {
                //Wiki
                entity = _wikiPageLocalService.fetchPage(modelClassPK);
                entityTitle = ((WikiPage) entity).getTitle();
            } else if (DLFileEntry.class.getName().equals(modelClassName)) {
                //Document
                entity = _dlFileEntryLocalService.fetchDLFileEntry(modelClassPK);
                entityTitle = ((DLFileEntry) entity).getTitle();
            } else {
                //Implement for custom types, if any
                return;
            }
            JSONObject extraData = JSONUtil.put("title", entityTitle);
            //Add "Like on comment" social activity
            SocialActivityManagerUtil.addActivity(
                    likeUserId, entity, SocialActivityKeys.TYPE_LIKE_ON_COMMENT,
                    extraData.toJSONString(), commentUserId);

        } catch (Exception e) {
            _log.error(e.getMessage(), e);
        }

        super.onAfterCreate(ratingsEntry);
    }

    @Reference(unbind = "-")
    public void setBlogsEntryLocalService(BlogsEntryLocalService blogsEntryLocalService) {
        this._blogsEntryLocalService = blogsEntryLocalService;
    }

    @Reference(unbind = "-")
    public void setWikiPageLocalService(WikiPageLocalService wikiPageLocalService) {
        this._wikiPageLocalService = wikiPageLocalService;
    }

    @Reference(unbind = "-")
    public void setDlFileEntryLocalService(DLFileEntryLocalService dlFileEntryLocalService) {
        this._dlFileEntryLocalService = dlFileEntryLocalService;
    }

    @Reference(unbind = "-")
    public void setMbMessageLocalService(MBMessageLocalService mbMessageLocalService) {
        this._mbMessageLocalService = mbMessageLocalService;
    }

    private BlogsEntryLocalService _blogsEntryLocalService;
    private WikiPageLocalService _wikiPageLocalService;
    private DLFileEntryLocalService _dlFileEntryLocalService;
    private MBMessageLocalService _mbMessageLocalService;

    private static Logger _log = LoggerFactory.getLogger(RatingsEntryListener.class.getName());

}
