package com.aimprosoft.liferay.document.web.social;

import com.aimprosoft.social.activity.util.SocialActivityKeys;
import com.liferay.asset.kernel.AssetRendererFactoryRegistryUtil;
import com.liferay.asset.kernel.model.AssetRenderer;
import com.liferay.asset.kernel.model.AssetRendererFactory;
import com.liferay.document.library.constants.DLPortletKeys;
import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.model.DLFolder;
import com.liferay.document.library.kernel.service.DLAppLocalService;
import com.liferay.petra.string.StringBundler;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.repository.capabilities.TrashCapability;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.security.permission.resource.ModelResourcePermission;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.ResourceBundleLoader;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.social.kernel.model.BaseSocialActivityInterpreter;
import com.liferay.social.kernel.model.SocialActivity;
import com.liferay.social.kernel.model.SocialActivityConstants;
import com.liferay.social.kernel.model.SocialActivityInterpreter;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.*;
import org.osgi.service.component.runtime.ServiceComponentRuntime;
import org.osgi.service.component.runtime.dto.ComponentDescriptionDTO;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

//todo:
@Component(
        property = {
                "javax.portlet.name=" + DLPortletKeys.DOCUMENT_LIBRARY,
                "model.class.name=com.liferay.document.library.kernel.model.DLFileEntry",
                "service.ranking:Integer=100"
        },
        service = SocialActivityInterpreter.class
)
public class AimDLFileActivityInterpreter extends BaseSocialActivityInterpreter {

    private static final int ADD_FILE_ENTRY = 1;
    private static final int UPDATE_FILE_ENTRY = 2;

    private volatile Set<ComponentDescriptionDTO> _disabledComponents = new HashSet<>();

    @Activate
    public void activate(
            ComponentContext componentContext, BundleContext bundleContext,
            Map<String, Object> config)
            throws Exception {

        String componentName = SocialActivityKeys.DL_FILE_ACTIVITY_INTERPRETER;

        Collection<ServiceReference<SocialActivityInterpreter>>
                serviceReferences =
                bundleContext.getServiceReferences(
                        SocialActivityInterpreter.class,
                        "(component.name=" + componentName + ")");

        for (ServiceReference serviceReference : serviceReferences) {
            Bundle bundle = serviceReference.getBundle();

            ComponentDescriptionDTO componentDescriptionDTO =
                    _serviceComponentRuntime.getComponentDescriptionDTO(
                            bundle, componentName);

            _serviceComponentRuntime.disableComponent(componentDescriptionDTO);

            _disabledComponents.add(componentDescriptionDTO);
        }
    }

    @Deactivate
    public void deactivate() {
        if (_disabledComponents == null || _disabledComponents.size() == 0) {
            return;
        }
        for (ComponentDescriptionDTO disabledComponent : _disabledComponents) {
            _serviceComponentRuntime.enableComponent(disabledComponent);
        }
        _disabledComponents.clear();
    }


    @Reference
    private ServiceComponentRuntime _serviceComponentRuntime;

    @Override
    public String[] getClassNames() {
        return _CLASS_NAMES;
    }

    @Override
    protected String getBody(
            SocialActivity activity, ServiceContext serviceContext)
            throws Exception {

        FileEntry fileEntry = _dlAppLocalService.getFileEntry(
                activity.getClassPK());

        if (fileEntry.isRepositoryCapabilityProvided(TrashCapability.class)) {
            TrashCapability trashCapability = fileEntry.getRepositoryCapability(
                    TrashCapability.class);

            if (trashCapability.isInTrash(fileEntry)) {
                return StringPool.BLANK;
            }
        }

        StringBundler sb = new StringBundler(3);

        AssetRendererFactory<?> assetRendererFactory =
                AssetRendererFactoryRegistryUtil.getAssetRendererFactoryByClassName(
                        DLFileEntry.class.getName());

        AssetRenderer<?> assetRenderer = assetRendererFactory.getAssetRenderer(
                fileEntry.getFileEntryId());

        String fileEntryLink = assetRenderer.getURLDownload(
                serviceContext.getThemeDisplay());

        sb.append(wrapLink(fileEntryLink, "download-file", serviceContext));

        sb.append(StringPool.SPACE);

        String folderLink = getFolderLink(fileEntry, serviceContext);

        folderLink = addNoSuchEntryRedirect(
                folderLink, DLFolder.class.getName(), fileEntry.getFolderId(),
                serviceContext);

        sb.append(wrapLink(folderLink, "go-to-folder", serviceContext));

        return sb.toString();
    }

    protected String getFolderLink(
            FileEntry fileEntry, ServiceContext serviceContext) {

        StringBundler sb = new StringBundler(6);

        sb.append(serviceContext.getPortalURL());
        sb.append(serviceContext.getPathMain());
        sb.append("/document_library/find_folder?groupId=");
        sb.append(fileEntry.getRepositoryId());
        sb.append("&folderId=");
        sb.append(fileEntry.getFolderId());

        return sb.toString();
    }

    @Override
    protected String getPath(
            SocialActivity activity, ServiceContext serviceContext) {

        return "/document_library/find_file_entry?fileEntryId=" +
                activity.getClassPK();
    }

    @Override
    protected ResourceBundleLoader getResourceBundleLoader() {
        return _resourceBundleLoader;
    }

    @Override
    protected String getTitle(SocialActivity activity, ServiceContext serviceContext) throws Exception {

        int activityType = activity.getType();

        if (activityType == SocialActivityKeys.TYPE_LIKE_ON_COMMENT) {

            //Custom - Like on comment

            String groupName = getGroupName(activity.getGroupId(), serviceContext);

            String titlePattern = getTitlePattern(groupName, activity);

            if (Validator.isNull(titlePattern)) {
                return null;
            }

            String link = getLink(activity, serviceContext);

            String entryTitle = getEntryTitle(activity, serviceContext);

            Object[] titleArguments = getTitleArguments(
                    groupName, activity, link, entryTitle, serviceContext);

            return serviceContext.translate(titlePattern, titleArguments);

        } else {

            //Default

            return super.getTitle(activity, serviceContext);
        }

    }

    @Override
    protected Object[] getTitleArguments(
            String groupName, SocialActivity activity, String link,
            String title, ServiceContext serviceContext)
            throws Exception {

        int activityType = activity.getType();
        if (activityType == SocialActivityConstants.TYPE_ADD_COMMENT) {
            String creatorUserName = getUserName(
                    activity.getUserId(), serviceContext);
            String receiverUserName = getUserName(
                    activity.getReceiverUserId(), serviceContext);

            return new Object[]{
                    groupName, creatorUserName, receiverUserName,
                    wrapLink(link, title)
            };
        } else if (activityType == SocialActivityKeys.TYPE_LIKE_ON_COMMENT) {
            String creatorUserName = getUserName(
                    activity.getUserId(), serviceContext);
            String receiverUserName = getUserName(
                    activity.getReceiverUserId(), serviceContext);
            return new Object[]{
                    creatorUserName,
                    receiverUserName,
                    wrapLink(link, title),
                    groupName
            };
        }

        return super.getTitleArguments(
                groupName, activity, link, title, serviceContext);
    }

    @Override
    protected String getTitlePattern(
            String groupName, SocialActivity activity) {

        int activityType = activity.getType();

        if (activityType == SocialActivityKeys.TYPE_LIKE_ON_COMMENT) {

            //Custom Social Activity Type: Like on Comment

            if (Validator.isNull(groupName)) {
                return "activity-document-library-file-add-comment";
            }
            return "activity-document-library-file-add-comment-in";

        } else if (activityType == ADD_FILE_ENTRY) {
            if (Validator.isNull(groupName)) {
                return "activity-document-library-file-add-file";
            }

            return "activity-document-library-file-add-file-in";
        } else if (activityType == UPDATE_FILE_ENTRY) {
            if (Validator.isNull(groupName)) {
                return "activity-document-library-file-update-file";
            }

            return "activity-document-library-file-update-file-in";
        } else if (activityType == SocialActivityConstants.TYPE_ADD_COMMENT) {
            if (Validator.isNull(groupName)) {
                return "activity-document-library-file-add-comment";
            }

            return "activity-document-library-file-add-comment-in";
        } else if (activityType == SocialActivityConstants.TYPE_MOVE_TO_TRASH) {
            if (Validator.isNull(groupName)) {
                return "activity-document-library-file-move-to-trash";
            }

            return "activity-document-library-file-move-to-trash-in";
        } else if (activityType ==
                SocialActivityConstants.TYPE_RESTORE_FROM_TRASH) {

            if (Validator.isNull(groupName)) {
                return "activity-document-library-file-restore-from-trash";
            }

            return "activity-document-library-file-restore-from-trash-in";
        }

        return null;
    }

    @Override
    protected String getViewEntryURL(
            String className, long classPK, ServiceContext serviceContext)
            throws Exception {

        return StringPool.BLANK;
    }

    @Override
    protected boolean hasPermissions(
            PermissionChecker permissionChecker, SocialActivity activity,
            String actionId, ServiceContext serviceContext)
            throws Exception {

        return _fileEntryModelResourcePermission.contains(
                permissionChecker, activity.getClassPK(), actionId);
    }

    @Reference(unbind = "-")
    protected void setDLAppLocalService(DLAppLocalService dlAppLocalService) {
        _dlAppLocalService = dlAppLocalService;
    }

    private static final String[] _CLASS_NAMES = {DLFileEntry.class.getName()};

    private DLAppLocalService _dlAppLocalService;

    @Reference(
            target = "(model.class.name=com.liferay.portal.kernel.repository.model.FileEntry)"
    )
    private ModelResourcePermission<FileEntry>
            _fileEntryModelResourcePermission;

    @Reference(
            policy = ReferencePolicy.DYNAMIC,
            policyOption = ReferencePolicyOption.GREEDY,
            target = "(bundle.symbolic.name=com.liferay.document.library.web)"
    )
    private volatile ResourceBundleLoader _resourceBundleLoader;

}