package com.aimprosoft.liferay.blogs.web.social;

import com.aimprosoft.social.activity.util.SocialActivityKeys;
import com.liferay.blogs.constants.BlogsPortletKeys;
import com.liferay.blogs.model.BlogsEntry;
import com.liferay.blogs.service.BlogsEntryLocalService;
import com.liferay.blogs.social.BlogsActivityKeys;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.security.permission.resource.ModelResourcePermission;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.FastDateFormatFactoryUtil;
import com.liferay.portal.kernel.util.ResourceBundleLoader;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.social.kernel.model.BaseSocialActivityInterpreter;
import com.liferay.social.kernel.model.SocialActivity;
import com.liferay.social.kernel.model.SocialActivityConstants;
import com.liferay.social.kernel.model.SocialActivityInterpreter;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.*;
import org.osgi.service.component.runtime.ServiceComponentRuntime;
import org.osgi.service.component.runtime.dto.ComponentDescriptionDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.Format;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Component(
        immediate = true,
        property = {
                "javax.portlet.name=" + BlogsPortletKeys.BLOGS,
                "service.ranking:Integer=100"
        },
        service = SocialActivityInterpreter.class
)
public class AimBlogsActivityInterpreter extends BaseSocialActivityInterpreter {

    private volatile Set<ComponentDescriptionDTO> _disabledComponents = new HashSet<>();

    @Activate
    public void activate(
            ComponentContext componentContext, BundleContext bundleContext,
            Map<String, Object> config)
            throws Exception {

        String componentName = SocialActivityKeys.BLOGS_ACTIVITY_INTERPRETER;

        Collection<ServiceReference<SocialActivityInterpreter>>
                serviceReferences =
                bundleContext.getServiceReferences(
                        SocialActivityInterpreter.class,
                        "(component.name=" + componentName + ")");

        for (ServiceReference serviceReference : serviceReferences) {
            Bundle bundle = serviceReference.getBundle();

            ComponentDescriptionDTO componentDescriptionDTO =
                    _serviceComponentRuntime.getComponentDescriptionDTO(
                            bundle, componentName);

            _serviceComponentRuntime.disableComponent(componentDescriptionDTO);

            _disabledComponents.add(componentDescriptionDTO);
        }
    }

    @Deactivate
    public void deactivate() {
        if (_disabledComponents == null || _disabledComponents.size() == 0) {
            return;
        }
        for (ComponentDescriptionDTO disabledComponent : _disabledComponents) {
            _serviceComponentRuntime.enableComponent(disabledComponent);
        }
        _disabledComponents.clear();
    }


    @Reference
    private ServiceComponentRuntime _serviceComponentRuntime;


    @Override
    public String[] getClassNames() {
        return _CLASS_NAMES;
    }

    @Override
    protected String getPath(
            SocialActivity activity, ServiceContext serviceContext) {

        return "/blogs/find_entry?entryId=" + activity.getClassPK();
    }

    @Override
    protected ResourceBundleLoader getResourceBundleLoader() {
        return _resourceBundleLoader;
    }

    @Override
    protected String getTitle(SocialActivity activity, ServiceContext serviceContext) throws Exception {

        int activityType = activity.getType();

        if (activityType == SocialActivityKeys.TYPE_LIKE_ON_COMMENT) {

            //Custom - Like on comment

            String groupName = getGroupName(activity.getGroupId(), serviceContext);

            String titlePattern = getTitlePattern(groupName, activity);

            if (Validator.isNull(titlePattern)) {
                return null;
            }

            String link = getLink(activity, serviceContext);

            String entryTitle = getEntryTitle(activity, serviceContext);

            Object[] titleArguments = getTitleArguments(
                    groupName, activity, link, entryTitle, serviceContext);

            return serviceContext.translate(titlePattern, titleArguments);

        } else {

            //Default

            return super.getTitle(activity, serviceContext);
        }

    }

    @Override
    protected Object[] getTitleArguments(
            String groupName, SocialActivity activity, String link,
            String title, ServiceContext serviceContext)
            throws Exception {

        String creatorUserName = getUserName(
                activity.getUserId(), serviceContext);
        String receiverUserName = getUserName(
                activity.getReceiverUserId(), serviceContext);

        BlogsEntry entry = _blogsEntryLocalService.getEntry(
                activity.getClassPK());

        String displayDate = "";

        if ((activity.getType() == BlogsActivityKeys.ADD_ENTRY) &&
                (entry.getStatus() == WorkflowConstants.STATUS_SCHEDULED)) {

            link = null;

            Format dateFormatDate =
                    FastDateFormatFactoryUtil.getSimpleDateFormat(
                            "MMMM d", serviceContext.getLocale(),
                            serviceContext.getTimeZone());

            displayDate = dateFormatDate.format(entry.getDisplayDate());
        }

        int activityType = activity.getType();
        if (activityType == SocialActivityKeys.TYPE_LIKE_ON_COMMENT) {
            return new Object[]{
                    creatorUserName,
                    receiverUserName,
                    wrapLink(link, title),
                    groupName
            };
        }

        return new Object[]{
                groupName, creatorUserName, receiverUserName, wrapLink(link, title),
                displayDate
        };
    }

    @Override
    protected String getTitlePattern(String groupName, SocialActivity activity)
            throws Exception {

        _log.info("AimBlogsActivityInterpreter => getTitlePattern invoked.");

        int activityType = activity.getType();

        if (activityType == SocialActivityKeys.TYPE_LIKE_ON_COMMENT) {

            //Custom Social Activity Type: Like on Comment

            if (Validator.isNull(groupName)) {
                return "activity-blogs-entry-like-comment";
            }
            return "activity-blogs-entry-like-comment-in";

        } else if ((activityType == BlogsActivityKeys.ADD_COMMENT) ||
                (activityType == SocialActivityConstants.TYPE_ADD_COMMENT)) {

            if (Validator.isNull(groupName)) {
                return "activity-blogs-entry-add-comment";
            }

            return "activity-blogs-entry-add-comment-in";
        } else if (activityType == BlogsActivityKeys.ADD_ENTRY) {
            BlogsEntry entry = _blogsEntryLocalService.getEntry(
                    activity.getClassPK());

            if (entry.getStatus() == WorkflowConstants.STATUS_SCHEDULED) {
                if (Validator.isNull(groupName)) {
                    return "activity-blogs-entry-schedule-entry";
                }

                return "activity-blogs-entry-schedule-entry-in";
            }

            if (Validator.isNull(groupName)) {
                return "activity-blogs-entry-add-entry";
            }

            return "activity-blogs-entry-add-entry-in";
        } else if (activityType == SocialActivityConstants.TYPE_MOVE_TO_TRASH) {
            if (Validator.isNull(groupName)) {
                return "activity-blogs-entry-move-to-trash";
            }

            return "activity-blogs-entry-move-to-trash-in";
        } else if (activityType ==
                SocialActivityConstants.TYPE_RESTORE_FROM_TRASH) {

            if (Validator.isNull(groupName)) {
                return "activity-blogs-entry-restore-from-trash";
            }

            return "activity-blogs-entry-restore-from-trash-in";
        } else if (activityType == BlogsActivityKeys.UPDATE_ENTRY) {
            if (Validator.isNull(groupName)) {
                return "activity-blogs-entry-update-entry";
            }

            return "activity-blogs-entry-update-entry-in";
        }

        return null;
    }

    @Override
    protected boolean hasPermissions(
            PermissionChecker permissionChecker, SocialActivity activity,
            String actionId, ServiceContext serviceContext)
            throws Exception {

        return _blogsEntryModelResourcePermission.contains(
                permissionChecker, activity.getClassPK(), actionId);
    }

    @Reference(unbind = "-")
    protected void setBlogsEntryLocalService(
            BlogsEntryLocalService blogsEntryLocalService) {

        _blogsEntryLocalService = blogsEntryLocalService;
    }

    private static final String[] _CLASS_NAMES = {BlogsEntry.class.getName()};

    private BlogsEntryLocalService _blogsEntryLocalService;

    @Reference(target = "(model.class.name=com.liferay.blogs.model.BlogsEntry)")
    private ModelResourcePermission<BlogsEntry>
            _blogsEntryModelResourcePermission;

    @Reference(
            policy = ReferencePolicy.DYNAMIC,
            policyOption = ReferencePolicyOption.GREEDY,
            target = "(bundle.symbolic.name=com.liferay.blogs.web)"
    )
    private volatile ResourceBundleLoader _resourceBundleLoader;


    private static Logger _log = LoggerFactory.getLogger(AimBlogsActivityInterpreter.class.getName());
}