package com.aimprosoft.liferay.wiki.web.social;


import com.aimprosoft.social.activity.util.SocialActivityKeys;
import com.liferay.petra.string.StringBundler;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.NoSuchModelException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portletfilerepository.PortletFileRepositoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.security.permission.resource.ModelResourcePermission;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ResourceBundleLoader;
import com.liferay.portal.kernel.util.URLCodec;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.social.kernel.model.BaseSocialActivityInterpreter;
import com.liferay.social.kernel.model.SocialActivity;
import com.liferay.social.kernel.model.SocialActivityConstants;
import com.liferay.social.kernel.model.SocialActivityInterpreter;
import com.liferay.wiki.constants.WikiPortletKeys;
import com.liferay.wiki.model.WikiPage;
import com.liferay.wiki.model.WikiPageResource;
import com.liferay.wiki.service.WikiPageLocalService;
import com.liferay.wiki.service.WikiPageResourceLocalService;
import com.liferay.wiki.social.WikiActivityKeys;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.*;
import org.osgi.service.component.runtime.ServiceComponentRuntime;
import org.osgi.service.component.runtime.dto.ComponentDescriptionDTO;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Component(
        property = {
                "javax.portlet.name=" + WikiPortletKeys.WIKI,
                "service.ranking:Integer=100"
        },
        service = SocialActivityInterpreter.class
)
public class AimWikiActivityInterpreter extends BaseSocialActivityInterpreter {

    private volatile Set<ComponentDescriptionDTO> _disabledComponents = new HashSet<>();

    @Activate
    public void activate(
            ComponentContext componentContext, BundleContext bundleContext,
            Map<String, Object> config)
            throws Exception {

        String componentName = SocialActivityKeys.WIKI_ACTIVITY_INTERPRETER;

        Collection<ServiceReference<SocialActivityInterpreter>>
                serviceReferences =
                bundleContext.getServiceReferences(
                        SocialActivityInterpreter.class,
                        "(component.name=" + componentName + ")");

        for (ServiceReference serviceReference : serviceReferences) {
            Bundle bundle = serviceReference.getBundle();

            ComponentDescriptionDTO componentDescriptionDTO =
                    _serviceComponentRuntime.getComponentDescriptionDTO(
                            bundle, componentName);

            _serviceComponentRuntime.disableComponent(componentDescriptionDTO);

            _disabledComponents.add(componentDescriptionDTO);
        }
    }

    @Deactivate
    public void deactivate() {
        if (_disabledComponents == null || _disabledComponents.size() == 0) {
            return;
        }
        for (ComponentDescriptionDTO disabledComponent : _disabledComponents) {
            _serviceComponentRuntime.enableComponent(disabledComponent);
        }
        _disabledComponents.clear();
    }


    @Reference
    private ServiceComponentRuntime _serviceComponentRuntime;


    @Override
    public String[] getClassNames() {
        return _CLASS_NAMES;
    }

    protected String getAttachmentTitle(
            SocialActivity activity, WikiPageResource pageResource,
            ServiceContext serviceContext)
            throws Exception {

        int activityType = activity.getType();

        if ((activityType == SocialActivityConstants.TYPE_ADD_ATTACHMENT) ||
                (activityType ==
                        SocialActivityConstants.TYPE_MOVE_ATTACHMENT_TO_TRASH) ||
                (activityType ==
                        SocialActivityConstants.TYPE_RESTORE_ATTACHMENT_FROM_TRASH)) {

            String link = null;

            FileEntry fileEntry = null;

            try {
                long fileEntryId = GetterUtil.getLong(
                        activity.getExtraDataValue("fileEntryId"));

                fileEntry = PortletFileRepositoryUtil.getPortletFileEntry(
                        fileEntryId);
            } catch (NoSuchModelException nsme) {

                // LPS-52675

                if (_log.isDebugEnabled()) {
                    _log.debug(nsme, nsme);
                }
            }

            String fileEntryTitle = activity.getExtraDataValue(
                    "fileEntryTitle");

            if ((fileEntry != null) && !fileEntry.isInTrash()) {
                StringBundler sb = new StringBundler(9);

                sb.append(serviceContext.getPathMain());
                sb.append("/wiki/get_page_attachment?p_l_id=");
                sb.append(serviceContext.getPlid());
                sb.append("&nodeId=");
                sb.append(pageResource.getNodeId());
                sb.append("&title=");
                sb.append(URLCodec.encodeURL(pageResource.getTitle()));
                sb.append("&fileName=");
                sb.append(fileEntryTitle);

                link = sb.toString();
            }

            return wrapLink(link, fileEntryTitle);
        }

        return StringPool.BLANK;
    }

    @Override
    protected String getPath(
            SocialActivity activity, ServiceContext serviceContext) {

        return "/wiki/find_page?pageResourcePrimKey=" + activity.getClassPK();
    }

    @Override
    protected ResourceBundleLoader getResourceBundleLoader() {
        return _resourceBundleLoader;
    }

    @Override
    protected String getTitle(SocialActivity activity, ServiceContext serviceContext) throws Exception {

        int activityType = activity.getType();

        if (activityType == SocialActivityKeys.TYPE_LIKE_ON_COMMENT) {

            //Custom - Like on comment

            String groupName = getGroupName(activity.getGroupId(), serviceContext);

            String titlePattern = getTitlePattern(groupName, activity);

            if (Validator.isNull(titlePattern)) {
                return null;
            }

            String link = getLink(activity, serviceContext);

            String entryTitle = getEntryTitle(activity, serviceContext);

            Object[] titleArguments = getTitleArguments(
                    groupName, activity, link, entryTitle, serviceContext);

            return serviceContext.translate(titlePattern, titleArguments);

        } else {

            //Default

            return super.getTitle(activity, serviceContext);
        }
    }

    @Override
    protected Object[] getTitleArguments(
            String groupName, SocialActivity activity, String link,
            String title, ServiceContext serviceContext)
            throws Exception {

        WikiPageResource pageResource =
                _wikiPageResourceLocalService.fetchWikiPageResource(
                        activity.getClassPK());

        if (pageResource == null) {
            return null;
        }

        String creatorUserName = getUserName(
                activity.getUserId(), serviceContext);

        title = wrapLink(link, title);

        String receiverUserName = "";
        try {
            long receiverUserId = activity.getReceiverUserId();
            User receiverUser = _userLocalService.getUser(receiverUserId);
            receiverUserName = receiverUser.getFullName();
        } catch (Exception e) {
            _log.error(e.getMessage(), e);
        }

        int activityType = activity.getType();
        if (activityType == SocialActivityKeys.TYPE_LIKE_ON_COMMENT) {
            return new Object[]{
                    creatorUserName,
                    receiverUserName,
                    title,
                    groupName
            };
        }

        return new Object[]{
                groupName, creatorUserName, title,
                getAttachmentTitle(activity, pageResource, serviceContext)
        };
    }

    @Override
    protected String getTitlePattern(
            String groupName, SocialActivity activity) {

        int activityType = activity.getType();

        if (activityType == SocialActivityKeys.TYPE_LIKE_ON_COMMENT) {

            //Custom Social Activity Type: Like on Comment

            if (Validator.isNull(groupName)) {
                return "activity-wiki-page-like-comment";
            }

            return "activity-wiki-page-like-comment-in";

        } else if ((activityType == WikiActivityKeys.ADD_COMMENT) ||
                (activityType == SocialActivityConstants.TYPE_ADD_COMMENT)) {

            if (Validator.isNull(groupName)) {
                return "activity-wiki-page-add-comment";
            }

            return "activity-wiki-page-add-comment-in";
        } else if (activityType == WikiActivityKeys.ADD_PAGE) {
            if (Validator.isNull(groupName)) {
                return "activity-wiki-page-add-page";
            }

            return "activity-wiki-page-add-page-in";
        } else if (activityType == SocialActivityConstants.TYPE_ADD_ATTACHMENT) {
            if (Validator.isNull(groupName)) {
                return "activity-wiki-page-add-attachment";
            }

            return "activity-wiki-page-add-attachment-in";
        } else if (activityType ==
                SocialActivityConstants.TYPE_MOVE_ATTACHMENT_TO_TRASH) {

            if (Validator.isNull(groupName)) {
                return "activity-wiki-page-remove-attachment";
            }

            return "activity-wiki-page-remove-attachment-in";
        } else if (activityType ==
                SocialActivityConstants.
                        TYPE_RESTORE_ATTACHMENT_FROM_TRASH) {

            if (Validator.isNull(groupName)) {
                return "activity-wiki-page-restore-attachment";
            }

            return "activity-wiki-page-restore-attachment-in";
        } else if (activityType == SocialActivityConstants.TYPE_MOVE_TO_TRASH) {
            if (Validator.isNull(groupName)) {
                return "activity-wiki-page-move-to-trash";
            }

            return "activity-wiki-page-move-to-trash-in";
        } else if (activityType ==
                SocialActivityConstants.TYPE_RESTORE_FROM_TRASH) {

            if (Validator.isNull(groupName)) {
                return "activity-wiki-page-restore-from-trash";
            }

            return "activity-wiki-page-restore-from-trash-in";
        } else if (activityType == WikiActivityKeys.UPDATE_PAGE) {
            if (Validator.isNull(groupName)) {
                return "activity-wiki-page-update-page";
            }

            return "activity-wiki-page-update-page-in";
        }

        return null;
    }

    @Override
    protected boolean hasPermissions(
            PermissionChecker permissionChecker, SocialActivity activity,
            String actionId, ServiceContext serviceContext)
            throws Exception {

        if (!_wikiPageModelResourcePermission.contains(
                permissionChecker, activity.getClassPK(), ActionKeys.VIEW)) {

            return false;
        }

        int activityType = activity.getType();

        if (activityType == WikiActivityKeys.UPDATE_PAGE) {
            WikiPageResource pageResource =
                    _wikiPageResourceLocalService.getPageResource(
                            activity.getClassPK());

            double version = GetterUtil.getDouble(
                    activity.getExtraDataValue("version"));

            WikiPage page = _wikiPageLocalService.getPage(
                    pageResource.getNodeId(), pageResource.getTitle(), version);

            if (!page.isApproved() &&
                    !_wikiPageModelResourcePermission.contains(
                            permissionChecker, activity.getClassPK(),
                            ActionKeys.UPDATE)) {

                return false;
            }
        }

        return true;
    }

    private static final String[] _CLASS_NAMES = {WikiPage.class.getName()};

    private static final Log _log = LogFactoryUtil.getLog(AimWikiActivityInterpreter.class);

    @Reference(
            policy = ReferencePolicy.DYNAMIC,
            policyOption = ReferencePolicyOption.GREEDY,
            target = "(bundle.symbolic.name=com.liferay.wiki.web)"
    )
    private volatile ResourceBundleLoader _resourceBundleLoader;

    @Reference
    private WikiPageLocalService _wikiPageLocalService;

    @Reference
    private UserLocalService _userLocalService;

    @Reference(target = "(model.class.name=com.liferay.wiki.model.WikiPage)")
    private ModelResourcePermission<WikiPage> _wikiPageModelResourcePermission;

    @Reference
    private WikiPageResourceLocalService _wikiPageResourceLocalService;

}