package com.aimprosoft.notification.activator;

import com.aimprosoft.common.util.AimConstants;
import com.aimprosoft.notification.util.AimNotificationConstants;
import com.liferay.journal.exception.NoSuchArticleException;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalFolderConstants;
import com.liferay.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.GroupConstants;
import com.liferay.portal.kernel.service.CompanyLocalServiceUtil;
import com.liferay.portal.kernel.service.GroupLocalServiceUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.Element;
import com.liferay.portal.kernel.xml.SAXReaderUtil;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class EmailNotificationActivator implements BundleActivator {

	private static final String WC_TITLE_PATH = "/META-INF/templates/site_group_notification_title.tmpl";
	private static final String WC_CONTENT_PATH = "/META-INF/templates/site_group_notification_content.tmpl";

	@Override
	public void start(BundleContext context) throws Exception {

		try {
			Company company = CompanyLocalServiceUtil.getCompanyByMx(AimConstants.COMPANY_MX);
			long companyId = company.getCompanyId();
			Group globalGroup = GroupLocalServiceUtil.getFriendlyURLGroup(companyId, GroupConstants.GLOBAL_FRIENDLY_URL);
			long groupGroupId = globalGroup.getGroupId();

			JournalArticle journalArticle = null;
			try {
				journalArticle = JournalArticleLocalServiceUtil.getArticleByUrlTitle(groupGroupId, AimNotificationConstants.SITE_GROUP_TEMPLATE_WC_URL);
			} catch (NoSuchArticleException nsae) {
				_log.warn(String.format("No article found with url title %s, creating default one.", AimNotificationConstants.SITE_GROUP_TEMPLATE_WC_URL));
			}

			if (journalArticle != null) {
				//Article had been already created
				return;
			}

			//Create article
			String articleTitle = null;
			String articleContent = null;

			Bundle bundle = context.getBundle();

			URL titleResource = bundle.getResource(WC_TITLE_PATH);
			URL contentResource = bundle.getResource(WC_CONTENT_PATH);
			try (InputStream titleInputStream = titleResource.openStream();
				 InputStream contentInputStream = contentResource.openStream();) {
				articleTitle = StringUtil.read(titleInputStream);
				articleContent = StringUtil.read(contentInputStream);
			}

			String xmlContent = getXmlContent(articleContent);

			Map<Locale, String> titleMap = new HashMap<>();
			titleMap.put(AimConstants.DEFAULT_LOCALE, articleTitle);
			Map<Locale, String> descriptionMap = new HashMap<>();

			long userId = company.getDefaultUser().getUserId();
			ServiceContext serviceContext = new ServiceContext();
			serviceContext.setCompanyId(companyId);
			serviceContext.setScopeGroupId(groupGroupId);
			serviceContext.setUserId(userId);

			//Create web content
			JournalArticle article = JournalArticleLocalServiceUtil.addArticle(
					userId,
					groupGroupId,
					JournalFolderConstants.DEFAULT_PARENT_FOLDER_ID,
					titleMap,
					descriptionMap,
					xmlContent,
					AimNotificationConstants.WC_STRUCTURE_KEY,
					AimNotificationConstants.WC_TEMPLATE_KEY,
					serviceContext
			);
			//Update URL title
			article.setUrlTitle(AimNotificationConstants.SITE_GROUP_TEMPLATE_WC_URL);
			article = JournalArticleLocalServiceUtil.updateJournalArticle(article);

			if (_log.isInfoEnabled()) {
				_log.info("Web Content for email template created successfully, articleId = " + article.getArticleId());
			}

		} catch (Exception e) {
			_log.error("Failed to check web content for email template, cause: " + e.getMessage());
		}
	}

	private String getXmlContent(String articleContent) {
		String languageId = "en_US";
		Element rootElement = SAXReaderUtil.createElement("root");
		rootElement.addAttribute("available-locales", "en_US");
		rootElement.addAttribute("default-locale", languageId);

		Element dynamicElement = SAXReaderUtil.createElement("dynamic-element");
		String instanceId = StringUtil.randomString(4);
		dynamicElement.addAttribute("name", "content");
		dynamicElement.addAttribute("type", "text_area");
		dynamicElement.addAttribute("index-type", "text");
		dynamicElement.addAttribute("instance-id", instanceId);

		Element dynamicContent = SAXReaderUtil.createElement("dynamic-content");
		dynamicContent.addAttribute("language-id", languageId);
		dynamicContent.addCDATA(articleContent);

		dynamicElement.add(dynamicContent);
		rootElement.add(dynamicElement);

		Document document = SAXReaderUtil.createDocument();
		document.add(rootElement);

		return document.asXML();
	}

	@Override
	public void stop(BundleContext bundleContext) throws Exception {
		if (_log.isInfoEnabled()) {
			_log.info("Stopped EmailNotificationActivator.");
		}
	}

	private static Logger _log = LoggerFactory.getLogger(EmailNotificationActivator.class.getName());
}